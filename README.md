# Citizens Advice user registration API

## Technical test

[Feel free to delete this section of the README.]

### Intro

We’re looking for the way you approach and solve problems, rather than perfect code from the outset - so please treat this as an opportunity to show us how you would approach this even if you aren't sure of the exact solution. The same test is given to people applying to any level in the organisation, and our expectations are adjusted accordingly.

The task should be committed to this git repository, with multiple commits, commit messages and pushed to the main branch on GitHub. We will add technical reviewers to the repo so there is no need for you to adjust access.

We would ask that you spend no more than 2 hours on this, even if that means you don’t complete the test fully.

### Deadline

Please return the technical test by 9am on **9 Sep 2021**.

If you have any questions or concerns about the test please don't hesitate to email Quinn Daley (pronouns: they/them) an email on <quinn.daley@citizensadvice.org.uk>.

### The task

Create a ruby based application for a user registration and authentication system. It should be designed as a REST-ful API with JSON only responses, there is no need for views.

* Users need to be able to register with a username, email address and password (there is no need to send a confirmation email).
* Users need to be able to authenticate with their username and password.
* When a user is successfully authenticated, the API needs to respond with a unique user token.
* Groups have a unique name and can have many users and users can belong to many groups.
* Some users have admin privileges and they need to be able to carry out the following actions using a valid user token:
    * Create groups.
    * Assign users to groups.
    
### During the interview

You’ll meet with a few team members that have technical backgrounds. You’ll walk us through the exercise you did, talk about how you approached the problem, what decisions you made, and why. We’ll ask you questions to assess your familiarity with the skills needed for this role. Following that, we’ll ask you to talk about some of your previous projects.
