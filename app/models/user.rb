class User < ApplicationRecord
  validates_uniqueness_of :username, :email
  validates_presence_of :username, :email, :password

  before_save :encrypt_password, :generate_api_key

  has_many :user_groups
  has_many :groups, through: :user_groups

  def self.authenticate(user, password)
    hashed_password = Digest::SHA256.hexdigest password
    if hashed_password == user.password
      user.api_key
    else
      'Invalid password'
    end
  end

private

  def encrypt_password
    self.password = Digest::SHA256.hexdigest self.password if self.password
  end

  def generate_api_key
    self.api_key = Digest::SHA256.hexdigest(DateTime.now.to_s)
  end
end
