class GroupsController < ApplicationController
  before_action :validate_user_token
  before_action :find_group, only: :update
  before_action :authenticate_admin

  def create
    if Group.create(group_params)
      render json: "Group created"
    end
  end

  def update
    if @group.update(group_params)
      render json: 'Group updated'
    end
  end

private

  def group_params
    params.require(:group).permit(:id, :name, user_ids: [])
  end

  def find_group
    @group = Group.find(group_params[:id])
  end
end
