class UsersController < ApplicationController
  def create
    if User.create(user_params)
      render json: "User created"
    end
  end

private

  def user_params
    params.require(:user).permit(:username, :email, :password, :admin)
  end
end
