class ApplicationController < ActionController::API
  def validate_user_token
    return if User.find_by(api_key: params[:api_key])
    render status: :unauthorized, json: "Unauthorised"
  end

  def authenticate_admin
    return if User.find_by(api_key: params[:api_key]).admin
    render status: :unauthorized, json: "Unauthorised"
  end
end
