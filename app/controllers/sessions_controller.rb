class SessionsController < ApplicationController
  def create
    user = User.find_by(username: session_params[:username])

    if user
      render json: User.authenticate(user, session_params[:password])
    end
  end

private

  def session_params
    params.require(:session).permit(:username, :password)
  end
end
