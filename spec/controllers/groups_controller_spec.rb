require 'rails_helper'

RSpec.describe GroupsController do
  let(:user) { User.create(email: 'test@test.com',
                           username: 'test',
                           password: 'Password',
                           admin: true) }

  let(:user1) { User.create(email: 'test1@test.com',
                           username: 'test1',
                           password: 'Password') }

  let(:user2) { User.create(email: 'test2@test.com',
                           username: 'test2',
                           password: 'Password') }

  let(:group) { Group.create(name: 'group1') }

  describe "POST create" do

    it 'should allow creation of groups from a valid user' do
      post :create, params: {api_key: user.api_key, group: { name: 'testgroup' }, remote: true }

      expect(response.status).to eq 200
      expect(response.body).to eq "Group created"
    end

    it 'should not allow creation of groups from an invalid user' do
      invalid_api_key = 'InvalidToken'

      post :create, params: {api_key: invalid_api_key, group: { name: 'testgroup' }, remote: true }

      expect(response.status).to eq 401
      expect(response.body).to eq "Unauthorised"
    end

    it 'should not allow non admin users to create groups' do
      post :create, params: {api_key: user1.api_key, group: { name: 'testgroup' }, remote: true }

      expect(response.status).to eq 401
      expect(response.body).to eq "Unauthorised"
    end
  end

  describe "PUT update" do
    it 'should allow users to be assigned to groups' do
      put :update, params: { api_key: user.api_key, group: { id: group.id, user_ids: [user1.id, user2.id] } }

      expect(group.users.count).to eq 2
      expect(response.body).to eq "Group updated"
    end
  end
end
