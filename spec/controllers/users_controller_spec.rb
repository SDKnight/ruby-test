require 'rails_helper'

RSpec.describe UsersController do
  describe "POST create" do
    it 'should allow creation of a user' do
      post :create, params: { user: { email: 'test@test.com', username: 'test', password: 'Password' }, remote: true }

      expect(response.status).to eq 200
      expect(response.body).to eq "User created"
      expect(User.find_by(email: 'test@test.com').admin).to eq false
    end

    it 'should allow creation of an admin user' do
      post :create, params: { user: { email: 'test@test.com',
                                      username: 'test',
                                      password: 'Password',
                                      admin: true }, remote: true }

      expect(response.status).to eq 200
      expect(response.body).to eq "User created"
      expect(User.find_by(email: 'test@test.com').admin).to eq true
    end
  end
end
