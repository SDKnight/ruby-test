require 'rails_helper'

RSpec.describe SessionsController do
  describe "POST create" do
    let(:user) { user = User.create(email: 'test@test.com',
                                    username: 'test',
                                    password: 'Password') }

    it 'should allow the login of a user' do
      user
      post :create, params: { session: { email: 'test@test.com', username: 'test', password: 'Password' }, remote: true }

      expect(response.status).to eq 200
      expect(response.body).to eq user.api_key
    end

    it 'should not allow login of an invalid user' do
      user
      post :create, params: { session: { email: 'test@test.com', username: 'test', password: 'WrongPassword' }, remote: true }

      expect(response.body).to eq "Invalid password"
    end
  end
end
