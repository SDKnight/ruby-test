require 'rails_helper'

RSpec.describe Group, type: :model do

  describe 'Database columns and data types' do
    it { should have_db_column(:name).of_type(:string) }
  end

  describe 'Validations' do
    it { should validate_uniqueness_of(:name) }
  end

  describe 'Associations' do
    it { should have_many(:users).through(:user_groups) }
  end
end
