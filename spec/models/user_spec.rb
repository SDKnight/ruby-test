require 'rails_helper'

RSpec.describe User, type: :model do

  let(:user) { user = User.create(email: 'test@test.com',
                                  username: 'test',
                                  password: 'Password') }

  let(:admin_user) { user = User.create(email: 'test@test.com',
                                        username: 'test',
                                        password: 'Password',
                                        admin: true) }

  describe 'Database columns and data types' do
    it { should have_db_column(:username).of_type(:string) }
    it { should have_db_column(:email).of_type(:string) }
    it { should have_db_column(:password).of_type(:string) }
    it { should have_db_column(:admin).of_type(:boolean).with_options(default: :false) }
  end

  describe 'Validations' do
    it { should validate_uniqueness_of(:username) }
    it { should validate_uniqueness_of(:email) }

    it { should validate_presence_of(:email) }
    it { should validate_presence_of(:username) }
    it { should validate_presence_of(:password) }
  end

  describe 'Associations' do
    it { should have_many(:groups).through(:user_groups) }
  end

  describe 'Password encryption' do
    it 'should encrypt the password for a user' do
      expect(user.password).to_not eq 'Password'
    end
  end

  describe 'User API Key' do
    it 'should generate a unique API key for the user to authentication requests' do
      expect(user.api_key).to_not eq nil
    end
  end

  describe 'Admin User' do
    it 'should allow creation of an admin user' do
      expect(admin_user.admin).to eq true
    end
  end
end
