Rails.application.routes.draw do
  resource :user, only: :create
  resource :session, only: :create
  resource :group, only: [:create, :update]
end
